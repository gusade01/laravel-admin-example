<?php

namespace App\Admin\Controllers;

use App\DataLingkungan;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class DataLingkunganController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Bank Data Lingkungan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new DataLingkungan());

        $grid->column('id', __('Id'));
        $grid->column('kategori', trans('Kategori'))->pluck('nama_kategori')->label();
        $grid->column('tahun', __('Tahun'));
        $grid->column('bulan', __('Bulan'));
        $grid->column('dokumen', __('Dokumen'));
        $grid->column('ket_dokumen', __('Keterangan Dokumen'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(DataLingkungan::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('kategori', trans('Kategori'))->as(function ($roles) {
            return $roles->pluck('nama_kategori');
        })->label();
        $show->field('tahun', __('Tahun'));
        $show->field('bulan', __('Bulan'));
        $show->field('dokumen', __('Dokumen'));
        $show->field('ket_dokumen', __('Keterangan Dokumen'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new DataLingkungan());
        $kategoriModel = config('admin.database.kategori_model');

        $permissionModel = config('admin.database.permissions_model');
        $roleModel = config('admin.database.roles_model');

        // $awaw = Kategori::all()->pluck('nama_kategori', 'id');
        $form->multipleSelect('kategori', trans('Kategori'))->options($kategoriModel::all()->pluck('nama_kategori', 'id'));

        // $form->multipleSelect('kategori_id')->options(Kategori::all()->pluck('nama_kategori', 'id'));

        $form->date('tahun', __('Tahun'))->default(date("Y-m-d"));
        $form->date('bulan', __('Bulan'))->default(date("Y-m-d"));
        // $form->file('dokumen', __('Dokumen'))->rules(['mimes:docx,xlsx,pdf','required']);
        $form->image('dokumen', __('Dokumen'))->rules('required');
        $form->textarea('ket_dokumen', __('Keterangan dokumen'));

        // $form->saved(function (Form $form){
        //     $form->kategori()->attach('kategori_id');
        // });


        // return env('APP_URL');
        return $form;
    }
}
